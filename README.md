# Développement d'application sur le cloud

Réalisé par :  
* Enora LANGARD
* Damien LORIOT
* Alexandre Ruaux

Lien du site :  https://fair-charmer-375313.ew.r.appspot.com/index4.html 
(Projet pétition --> Accueil pétition)

# TinyPetition
** Initialisation du datastore local**
Toutes les fonctionnalités liées à des appels API se situent dans le fichier ScoreEndPoint
Pour générer les données des pétitions il faut cliquer lancer la page 
lien: https://fair-charmer-375313.ew.r.appspot.com/PetitionTP
(Data Queries for managing Petitions --> install petition like data (long))

**Kind du DataStore**

```
indexes:
- kind: Petition 
  properties: 
    - name: nbSignature
      direction: desc
    - name: titre
- kind: Petition 
  properties:
    - name: createur
    - name: dateCreation
      direction: desc
- kind: Petition 
  properties:
    - name: signataires
    - name: dateCreation
      direction: desc

```

**Fonctionnalités fonctionnelles**

* Un utilisateur peut se connecter via son compte google.

* Un utilisateur peut voir l'ensemble des pétitions existantes depuis la page d'accueil du site.
* Un utilisateur peut créer une pétition :
    * Si l'utilisateur ne s'est pas identifié avec google, il peut quand même créer une pétition mais le créateur de la pétition sera "U0".
* Un utilisateur peut signer une pétition (Via le bouton Infos > Signer).
    * Si il ne s'est pas identifié avec google alors l'on rajoute l'utilisateur par défaut "U0" comme signataire.
    * Un même utilisateur ne peut pas signer 2 fois la même pétition. 
    * Un utilisateur ne peut pas retirer sa signature d'une pétition. 
* Un utilisateur peut voir les pétitions dont il est le créateur. Les pétitions sont triés par date.
* Un utilisateur peut voir les pétitions qu'il a signé. Les pétitions sont triés par date.
* L'affichage des 100 pétitions trié par date.

**Fonctionnalités non fonctionnelles**

* L'ajout et l'utilisation des tags d'une pétition.
* L'affichage des 100 pétitions avec le nombre de signataires le plus élevé et trié par date.




